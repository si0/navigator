// todo(?): n/e(x)hentai support (get cookies, sessionid)
package main

import (
	"fmt"
	"net/url"
	"os"
	"os/exec"
	"regexp"
	"runtime"
	"strings"

	"github.com/anaskhan96/soup"
)

var query = os.Args[1]

func navig8(qry string) {
	var err error
	switch runtime.GOOS {
	case "linux":
		err = exec.Command("xdg-open", qry).Start()
	case "windows":
		err = exec.Command("rundll32", "url.dll,FileProtocolHandler", qry).Start()
	case "darwin":
		err = exec.Command("open", qry).Start()
	default:
		err = fmt.Errorf("unsupported platform")
	}
	if err != nil {
		fmt.Println(err)
	}
}

func ff(links []soup.Root) string {
	for _, link := range links {
		if link.Attrs()["property"] == "og:title" {
			title := link.Attrs()["content"]
			return title
		}
	}
	return ""
}

func main() {
	u, _ := url.Parse(query)
	site := u.Host
	mylink := u.Path

	vidsite := map[string]bool{
		"www.youtube.com":     true,
		"youtube.com":         true,
		"youtu.be":            true,
		"vimeo.com":           true,
		"v.redd.it":           true,
		"clips.twitch.tv":     true,
		"twitch.tv":           true,
		"streamable.com":      true,
		"banned.video":        true,
		"odysee.com":          true,
		"www.dailymotion.com": true,
		"www.redgifs.com":     true,
	}

	if vidsite[site] {
		exec.Command("mpv", "--screen=1", query).Start()
		os.Exit(0)
	}

	music := map[string]bool{
		"soundcloud.com": true,
		"vocaroo.com":    true,
	}

	if music[site] {
		exec.Command("mpv", "--force-window", "--screen=1", query).Start()
		os.Exit(0)
	}

	booru := map[string]bool{
		"yande.re":           true,
		"danbooru.donmai.us": true,
		"gelbooru.com":       true,
		"rule34.xxx":         true,
	}

	if booru[site] {
		resp, err := soup.Get(query)
		if err != nil {
			os.Exit(1)
		}
		doc := soup.HTMLParse(resp)
		links := doc.FindAll("meta")
		dnbrt := ff(links)
		mpvt := "--title=" + dnbrt
		for _, link := range links {
			if link.Attrs()["property"] == "og:image" {
				ogimg := link.Attrs()["content"]
				exec.Command("mpv", "--config-dir=C:\\Users\\User\\AppData\\Roaming\\mvi", "--screen=1", "--pause", mpvt, ogimg).Start()
				os.Exit(0)
			}
		}
	}

	pixiv := map[string]bool{
		"i.pximg.net":   true,
		"pbs.twimg.com": true,
		"i.redd.it":     true,
	}

	if pixiv[site] {
		pxhdr := "--http-header-fields=Referer: https://www.pixiv.net/"
		exec.Command("mpv", "--config-dir=C:\\Users\\User\\AppData\\Roaming\\mvi", "--screen=1", "--pause", pxhdr, query).Start()
		os.Exit(0)
	}

	tiktok := map[string]bool{
		"vm.tiktok.com":  true,
		"www.tiktok.com": true,
	}

	if tiktok[site] {
		tiktoklink, _ := exec.Command("yt-dlp", "--abort-on-error", "--get-url", query).CombinedOutput()
		tiktokstr := string(tiktoklink)
		exec.Command("mpv", "--loop=inf", tiktokstr).Start()
		os.Exit(0)
	}

	insta := map[string]bool{
		"instagram.com":     true,
		"www.instagram.com": true,
	}

	if insta[site] {
		tknzd := strings.Split(mylink, "/")
		if (tknzd[1] == "p") || (tknzd[1] == "reel") || (tknzd[1] == "tv") {
			instalink, _ := exec.Command("yt-dlp", "--abort-on-error", "--no-warnings", "--get-url", query).CombinedOutput()
			instastr := string(instalink)
			itkn := strings.Split(instastr, "\n")
			xy := strings.Join(itkn, "\" \"")
			yy := strings.TrimSuffix(xy, "\"")
			dy := "\"" + yy
			dz := strings.Trim(dy, "\"")
			exec.Command("mpv", "--loop=inf", dz).Start()
			os.Exit(0)
		}
	}

	image, _ := regexp.MatchString(".*\\.(?i:png|jpg|jpeg|webp|avif)$", mylink)
	webm, _ := regexp.MatchString(".*\\.(?i:webm|mp4|mkv|mov|gif|swf)$", mylink)

	if image {
		exec.Command("mpv", "--config-dir=C:\\Users\\User\\AppData\\Roaming\\mvi", "--screen=1", "--pause", query).Start()
	} else if webm {
		exec.Command("mpv", "--loop=inf", "--screen=1", query).Start()
	} else {
		navig8(query)
	}
}
